# Serial Communication with callback subscription: #

## serialcb/ ##
java/maven package depending on jSerialComm to communicate
with a serial port. It allows to subscript callbacks that
will execute in order of subscription when recieving
data from the specified serial port (using port descriptor).

basically you can only do 2 things at the moment:
- constructing SPListener with a given serial buffer size and a string
indicating the serial port descriptor (com). This is done via the constructor: `SPListener(SPListener(String prot_descriptor, int buffsize)`
  - Default baud rate is 9600, but there is an SPListener constructor overload that allows specifying baud_rate via a third parameter:
  `SPListener(String prot_descriptor, int buffsize, int baud_rate)`
- subscribing callbacks to an instance of SPListener `scb.subscriptCB(()->{})` where the argument is a lambda function and scb
an instance of SPListener.

## cmd_serialcb/ ##
sample programme of how serialcb can be used.

in Linux (and probably other unix-like) you can use socat to emulate
a serial port as:
`socat -d -d pty,raw,echo=0 pty,raw,echo=0`
it should output something like:
```
yyyy/mm/dd hh:mm:ss socat[12609] N PTY is /dev/pts/5
yyyy/mm/dd hh:hh:ss socat[12609] N PTY is /dev/pts/6
yyyy/mm/dd hh:hh:ss socat[12609] N starting data transfer loop with FDs [5,5] and [7,7]
```
1) Run the above socat command in a terminal
2) In another terminal, run cmd_serialcb with one of the pts as argument (say `/dev/pts/5` from the output above)
3) In yet another terminal, echo something into the remaining pts (following the example, `echo "hello" > /dev/pts/6` from the output above)
