package com.serialCB;

/* Java imports */
import java.util.Objects;
import java.util.Vector;
import java.io.IOException;
import java.util.function.Consumer;

/* Third-party imports */
import com.fazecast.jSerialComm.*;

/*
Possibility: Extending SerialListenerCB with priority queues or other algorithms via
an interface as:

interface SerialPortCB extends SerialPortDataListener {
    public void subscriptCB(Consumer<T> new_CB);

    @Override
    public int getListeningEvents();
    @Override
    public void serialEvent();
}
*/

public class SPListener implements SerialPortDataListener {
    private final byte[] buff;
    private final Vector<Consumer<String>> cbs;

    /* constructor with default baud rate (9600) */
    public SPListener(String port_descriptor, int buffsize) throws IOException, IllegalArgumentException {
        if (buffsize < 1)
            throw new IllegalArgumentException("Illegal Argument @ SPListener() constructor: buffer size for serial comm less than 1 (buffsize < 1)");

        SerialPort serialport = SerialPort.getCommPort(port_descriptor);
        serialport.openPort();
        if (!serialport.isOpen())
            throw new IOException("Unable to open serial port ".concat(port_descriptor).concat(" @ SPListener() constructor"));

        buff = new byte[buffsize];
        cbs = new Vector<>();
        serialport.addDataListener(this);
    }

    /* constructor specifying baud_rate (3rd parameter) */
    public SPListener(String port_descriptor, int buffsize, int baud_rate) throws IOException, IllegalArgumentException {
        if (buffsize < 1)
            throw new IllegalArgumentException("Illegal Argument @ SPListener() constructor: buffer size for serial comm less than 1 (buffsize < 1)");

        if (baud_rate < 1)
            throw new IllegalArgumentException("Illegal Argument @ SPListener() constructor: baud rate can not be negative");

        SerialPort serialport = SerialPort.getCommPort(port_descriptor);
        serialport.openPort();
        if (!serialport.isOpen())
            throw new IOException("Unable to open serial port ".concat(port_descriptor).concat(" @ SPListener() constructor"));
        serialport.setBaudRate(baud_rate); /* setBaudRate is useless if the port is not open */

        buff = new byte[buffsize];
        cbs = new Vector<>();
        serialport.addDataListener(this);
    }

    public void subscriptCB(Consumer<String> new_cb) throws IllegalArgumentException {
        if (Objects.equals(new_cb, null))
            throw new IllegalArgumentException("SPListener.subscriptCB(): Can't subscript null Consumer<String>");
        cbs.add(new_cb);
    }

    private void callCBS() {
        /* buff can't be null since it would've thrown exception in construct */
        String param = new String(buff);
        for (Consumer<String> cb : cbs)
            cb.accept(param);
    }

    @Override
    public int getListeningEvents() {
        return SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
    }

    @Override
    public void serialEvent(SerialPortEvent event) {
        event.getSerialPort().readBytes(buff, buff.length);
        callCBS();
    }
}