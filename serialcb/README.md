## Compiling this stuff... ##

First things first, sorry for making you use maven.
If you don't have maven, install it via your OS/distro package manager (or download a zip if on Windows idk)

Then run `mvn assembly:assembly` (`mvn package` sometimes works but for some reason it does not always use maven-assembly)
from the folder where this README.md lays. And that's it, import this into your projects
or stuff. It is using maven-assembly to compile dependencies into the output jar, so there should be no problemos.
