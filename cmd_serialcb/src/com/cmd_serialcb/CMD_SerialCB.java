package com.cmd_serialcb;

import java.io.IOException;
import com.serialCB.SPListener;

public class CMD_SerialCB {
    public static void main(String[] args) {
        int bufsize = 12; /* arbitrary size for buffer size */
        if (args.length < 1)
            System.exit(-1); /* Error output */

        try {
            SPListener scb = new SPListener(args[0], bufsize);
            scb.subscriptCB((String str) -> System.out.println("Callback 1 (normal):".concat(str)));
            scb.subscriptCB((String str) -> System.out.println("Callback 2 (toUpper):".concat(str.toUpperCase())));
            scb.subscriptCB((String str) -> System.out.println("Callback 3 (toLower):".concat(str.toLowerCase())));
        } catch (IOException io_exception) {
            System.out.println("IO_Exception");
        }
    }
}